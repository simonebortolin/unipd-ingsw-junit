public interface Temperature {
    public double getCelsius();

    public void setCelsius(double celsius);

    public double getKelvin();

    public void setKelvin(double kelvin);
}
